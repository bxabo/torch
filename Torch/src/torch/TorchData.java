package torch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;

import javafx.scene.Node;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Region;
import javafx.scene.shape.Line;
import models.Field;
import models.LineSeries;
import models.Relationship;
import models.Schema;
import models.Table;

public class TorchData {

	private static TorchData torchData = null;

	Map<String, Schema> schemasById = new TreeMap<>();
	Map<String, Table> tablesById = new TreeMap<>();
	Map<String, Table> tablesByDatabaseName = new TreeMap<>();
	Map<String, Relationship> relationships = new TreeMap<>();
	Map<String, Table> displayedTables = new HashMap<>();
	/**
	 * Key is Field.getFieldId
	 */
	Map<String, Field> displayedFields = new HashMap<>();
	Map<String, Relationship> displayedRelationships = new HashMap<>();
	ArrayList<LineSeries<? extends Region>> displayedLineSeries = new ArrayList<>();
	ArrayList<Line> base = new ArrayList<>();
	Pane centerPane;

	private TorchData() {
		// prevent instantiation
	}

	public static TorchData getInstance() {
		if(torchData == null) {
			torchData = new TorchData();
		}
		return torchData;
	}

	public Map<String, Schema> getSchemas() {
		return schemasById;
	}

	public void setSchemas(Map<String, Schema> schemas) {
		this.schemasById = schemas;
	}

	public Map<String, Table> getTables() {
		return tablesById;
	}

	public void setTables(Map<String, Table> tables) {
		tablesById = tables;
	}
	// build sorted list by database name
	public Map<String, Table> getSortedTables() {
		if(tablesByDatabaseName.size() != tablesById.size()) {
			tablesByDatabaseName.clear();
			for(Map.Entry<String, Table> entry : tablesById.entrySet()) {
				tablesByDatabaseName.put(entry.getValue().getDatabaseName(), entry.getValue());
			}
		}
		return tablesByDatabaseName;
	}

	public Map<String, Relationship> getRelationships() {
		return relationships;
	}

	public void setRelationships(Map<String, Relationship> relationships) {
		this.relationships = relationships;
	}

	public void addDisplayedTable(String key, Table value) {
		displayedTables.put(key, value);
	}

	public Map<String, Table> getDisplayedTables() {
		return displayedTables;
	}

	public boolean isTableDisplayed(String tableId){
		if(getDisplayedTables().containsKey(tableId)){
			return true;
		}
		return false;
	}

	public void removeDisplayedTable(String key) {
		displayedTables.remove(key);
	}

	public void addDisplayedField(String key, Field value) {
		displayedFields.put(key, value);
	}

	public Map<String, Field> getDisplayedFields() {
		return displayedFields;
	}

	public void removeDisplayedField(String key) {
		displayedFields.remove(key);
	}
	public void addDisplayedRelationship(String key, Relationship value) {
		displayedRelationships.put(key, value);
	}

	public Map<String, Relationship> getDisplayedRelationships() {
		return displayedRelationships;
	}

	public void removeDisplayedRelationship(String key) {
		displayedRelationships.remove(key);
	}

	public Pane getCenterPane() {
		return centerPane;
	}

	public void setCenterPane(Pane centerPane) {
		this.centerPane = centerPane;
	}


	public void addLine(Line t) {
		getCenterPane().getChildren().add(t);
		base.add(t);
		t.toBack();
	}

	public void addLines(List<Line> lines){
		ListIterator<Line> iter = lines.listIterator();
		while(iter.hasNext()) {
			Line l = iter.next();
			addLine(l);
		}
	}

	public void addLineSeries(LineSeries<? extends Region> series) {
		addLines(series.lines());
		displayedLineSeries.add(series);
	}

	public ArrayList<LineSeries<? extends Region>> getLineSeriesConnectedTo(String tableId) {
		ArrayList<LineSeries<? extends Region>> l = new ArrayList<>();
		for(LineSeries<? extends Region> serie : displayedLineSeries) {
			if(tableId.equals(serie.getStartTableId()) || tableId.equals(serie.getEndTableId())){
				l.add(serie);
			}
		}
		return (l.size() != 0) ? l : null;
	}

	public void removeLinesConnectedTo(String tableId) {
		Iterator<LineSeries<? extends Region>> it = displayedLineSeries.iterator();
		while(it.hasNext()) {
			LineSeries<? extends Region> serie = it.next();
			if(tableId.equals(serie.getStartTableId()) || tableId.equals(serie.getEndTableId())){
				removeLinesInSeries(serie);
				it.remove();
			}
		}
	}

	public void removeLinesInSeries(LineSeries<?> series) {
		Iterator<Node> nodesIt = getCenterPane().getChildren().iterator();
		while(nodesIt.hasNext()) {
			Node node = nodesIt.next();
			if(Line.class.equals(node.getClass())){
				Line nodeLine = (Line) node;
				for(Line line : series.lines()) {
						if(line == nodeLine) {
							nodesIt.remove();
					}
				}
			}
		}
	}
}
