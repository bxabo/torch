package torch;

import java.io.IOException;
import java.util.zip.ZipInputStream;

import dataloader.AspenDataLoader;
import dataloader.DataLoader;
import dataloader.DataVerifier;
import gui.RootController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class TorchViewer extends Application {

	private Stage primaryStage;
	private BorderPane rootLayout;
	private RootController rootController;

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		this.primaryStage = primaryStage;
		this.primaryStage.setTitle("Torch");
		Image torchIcon = new Image(TorchViewer.class.getResourceAsStream("/resources/icons/flashlight.png"));
		String torchBackground = TorchViewer.class.getResource("/resources/icons/flashlight.png").toExternalForm();
		this.primaryStage.getIcons().add(torchIcon);
		initRootLayout();
		rootLayout.setStyle("-fx-background-color: gray;" +
				"-fx-background-image: url(" + torchBackground + ");" +
				"-fx-background-repeat: stretch;" +
				"-fx-background-position: center center;");

		// load tables, fields, and relationships
		// TODO : Select file(s)?  Detect Type?  Perhaps file managing methods in the DataLoader class?

		// MSSQL Data loader
		// String fileName = "/resources/AdventureWorks2012.sql";
		// InputStream stream = TorchViewer.class.getResourceAsStream(fileName);
		// DataLoader dataLoader = new MsSqlFileDataLoader();

		// Aspen Data Loader		
		String fileName = "/resources/Torch-data-V5-6-0-57.zip";
		ZipInputStream stream = new ZipInputStream(TorchViewer.class.getResourceAsStream(fileName));
		DataLoader dataLoader = new AspenDataLoader();

		/* TODO: Threading - Gui stuff shouldn't need to wait on data load, but we need to 
		 * signal when we are completely loaded before allowing user input. 
		 */
		dataLoader.loadData(stream);
		
		// verify loaded data and connections
		DataVerifier.verify();
		
		rootController.setDropDownData(TorchData.getInstance().getTables().keySet());
	}

	public void initRootLayout() {
		try {
			// Load root layout from fxml
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(TorchViewer.class.getResource("/gui/Root.fxml"));
			rootLayout = (BorderPane) loader.load();
			rootController = loader.getController();

			// Show the scene
			Scene scene = new Scene(rootLayout);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(IOException ioe) {
			ioe.printStackTrace();
		}
	}
}
