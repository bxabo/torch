/**
 * This is intended to be run inside the Aspen application to generate the files necessary for viewing
 * the tables, fields, and relationships - an application view of the schema
 */
package resources;

import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.ojb.broker.query.Criteria;

import com.follett.fsc.core.framework.persistence.ColumnQuery;
import com.follett.fsc.core.k12.beans.DataTable;
import com.follett.fsc.core.k12.beans.ReportQueryIterator;
import com.follett.fsc.core.k12.tools.ToolJavaSource;
import com.follett.fsc.core.k12.web.AppGlobals;
import com.x2dev.utils.DataGrid;

public class TorchDataExtractor extends ToolJavaSource {
    HashMap<String, String> tableClassMap;
    DataGrid tblGrid;
    DataGrid fldGrid;
    DataGrid relGrid;
    
    @Override
    protected void run() throws Exception {
        tableClassMap = getTableClasses();
        tblGrid = getTables();
        fldGrid = getFields();
        relGrid = getRelationships();

        ZipOutputStream zip = new ZipOutputStream(getResultHandler().getOutputStream());
        zip.putNextEntry(new ZipEntry("TABLE-CLASSES.CSV"));
        for(Map.Entry<String, String> entry :  tableClassMap.entrySet()) {
            String table = entry.getKey();
            String cls = entry.getValue();
            if(table != null)
                table = table.trim();
            if(cls != null)
                cls = cls.trim();
            zip.write((table + "," + cls + "\n").getBytes(Charset.forName("UTF-8")));
        }
        zip.closeEntry();
        zip.putNextEntry(new ZipEntry("TABLES.TXT"));
        zip.write(tblGrid.format(false, false, false).getBytes(Charset.forName("UTF-8")));
        zip.closeEntry();
        zip.putNextEntry(new ZipEntry("FIELDS.TXT"));
        zip.write(fldGrid.format(false, false, false).getBytes(Charset.forName("UTF-8")));
        zip.closeEntry();
        zip.putNextEntry(new ZipEntry("RELATIONSHIPS.TXT"));
        zip.write(relGrid.format(false, false, false).getBytes(Charset.forName("UTF-8")));
        zip.closeEntry();
        zip.finish();
    }
    
    @Override
    public String getCustomFileName() {
        return "elements-data-V" + AppGlobals.getVersion().replace(".", "-") + ".zip";
    }
    
    public DataGrid getGridFromSql(String sql) {
        DataGrid grid = new DataGrid();
        Statement s = null;
        try {
            Connection conn = getBroker().borrowConnection();
            s = conn.createStatement();
            ResultSet rs = s.executeQuery(sql.toString());
            ResultSetMetaData md = rs.getMetaData();
            int columnCount = md.getColumnCount();
            while(rs.next()) {
                grid.append();
                for(int x=1; x<=columnCount; x++) {
                    String value = rs.getString(x);
                    if(value != null)
                        value = value.trim();
                    grid.set(md.getColumnLabel(x), value);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Unable to execute query: " + sql.toString(), e);
        } finally {
            try {
                s.close();
            } catch (SQLException e) {
                // close(d) enough
            }
            getBroker().returnConnection();
        }
        grid.beforeTop();
        return grid;
    }

    public HashMap<String, String> getTableClasses() {
        HashMap<String, String> map = new HashMap<>();
        ColumnQuery query = new ColumnQuery(DataTable.class, new String[] {DataTable.COL_DATABASE_NAME, DataTable.COL_CLASS_NAME}, new Criteria());
        ReportQueryIterator iterator = getBroker().getReportQueryIteratorByQuery(query);
        try {
            while(iterator.hasNext()) {
                Object[] row = (Object[]) iterator.next();
                map.put((String)row[0], (String)row[1]);
            }
        } finally {
            iterator.close();
        }
        return map;
    }

    public DataGrid getTables() {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT");
        sql.append("  TBL_OID AS TABLE_OID,");
        sql.append("  TBL_CLASS_NAME AS CLASS_NAME,");
        sql.append("  TBL_DATABASE_NAME AS DATABASE_NAME,");
        sql.append("  TBL_OBJECT_PREFIX AS PREFIX,");
        sql.append("  TBD_USER_NAME AS USER_NAME,");
        sql.append("  TBL_PARENT_CLASS_NAME AS PARENT_CLASS,");
        sql.append("  CASE");
        sql.append("   WHEN TBL_CLASS_NAME LIKE \"%.core.%\" THEN \"core\"");
        sql.append("   WHEN TBL_CLASS_NAME LIKE \"%.sis.%\" THEN \"sis\"");
        sql.append("   ELSE TBL_CLASS_NAME");
        sql.append("  END AS CODEBASE");
        sql.append(" FROM DATA_TABLE TBL");
        sql.append(" JOIN DATA_TABLE_CONFIG TBD ON TBD.TBD_TBL_OID = TBL_OID");
        return getGridFromSql(sql.toString());
    }

    public DataGrid getFields() {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT");
        sql.append("  FLD_TBL_OID AS TABLE_OID,");
        sql.append("  SUBSTRING(FLD_OID, 1, 9) AS FIELD_OID,");
        sql.append("  COUNT(*) AS FIELD_COUNT,");
        sql.append("  FLD_JDBC_TYPE AS JDBC_TYPE,");
        sql.append("  FLD_DATABASE_LENGTH AS DATABASE_LEN,");
        sql.append("  FLD_JAVA_TYPE AS JAVA_TYPE,");
        sql.append("  REPLACE(FLD_JAVA_NAME, RIGHT(FLD_JAVA_NAME, 3), '') AS JAVA_NAME,");
        sql.append("  REPLACE(FLD_DATABASE_NAME, RIGHT(FLD_DATABASE_NAME, 4), '') AS DATABASE_NAME");
        sql.append(" FROM DATA_FIELD FLD");
        sql.append(" JOIN DATA_FIELD_CONFIG FDD ON FDD.FDD_FLD_OID = FLD_OID");
        sql.append(" WHERE SUBSTRING(FLD_OID, 4, 6) IN ('FieldA','FieldB', 'FieldC', 'FieldD', 'FieldE')");
        sql.append(" GROUP BY SUBSTRING(FLD_OID, 1, 9)");
        sql.append(" UNION ALL ");
        sql.append(" SELECT");
        sql.append("  FLD_TBL_OID AS TABLE_OID,");
        sql.append("  FLD_OID AS FIELD_OID,");
        sql.append("  '0' AS FIELD_COUNT,");
        sql.append("  FLD_JDBC_TYPE AS JDBC_TYPE,");
        sql.append("  FLD_DATABASE_LENGTH AS DATABASE_LEN,");
        sql.append("  FLD_JAVA_TYPE AS JAVA_TYPE,");
        sql.append("  FLD_JAVA_NAME AS JAVA_NAME,");
        sql.append("  FLD_DATABASE_NAME AS DATABASE_NAME");
        sql.append(" FROM DATA_FIELD FLD");
        sql.append(" JOIN DATA_FIELD_CONFIG FDD ON FDD.FDD_FLD_OID = FLD_OID");
        sql.append(" WHERE SUBSTRING(FLD_OID, 4, 6) NOT IN ('FieldA','FieldB', 'FieldC', 'FieldD', 'FieldE')");
        return getGridFromSql(sql.toString());
    }

    public DataGrid getRelationships() {
        StringBuilder sql = new StringBuilder();
        sql.append(" SELECT");
        sql.append("  REL_OID,");
        sql.append("  REL_TBL_OID_PRIMARY AS PRIMARY_TABLE,");
        sql.append("  REL_RELATION_TYPE_PRIMARY AS PRIMARY_TYPE,");
        sql.append("  REL_TBL_OID_RELATED AS RELATED_TABLE,");
        sql.append("  REL_RELATION_TYPE_RELATED AS RELATED_TYPE,");
        sql.append("  REL_IDX_OID_PRIMARY AS PRIMARY_TABLE_IDX,");
        sql.append("  REL_IDX_OID_RELATED AS RELATED_TABLE_IDX,");
        sql.append("  REL.REL_JAVA_NAME AS JAVA_NAME,");
        sql.append("  REL.REL_FLD_OID_ORDER_BY AS ORDER_BY,");
        sql.append("  REL.REL_SORT_DIRECTION AS SORT_DIRECTION");
        sql.append(" FROM DATA_RELATION REL");
        sql.append(" JOIN DATA_RELATION_DISTRICT RLD ON RLD.RLD_REL_OID = REL_OID");
        sql.append(" JOIN DATA_TABLE TBL ON REL.REL_TBL_OID_PRIMARY = TBL.TBL_OID");
        sql.append(" JOIN DATA_TABLE_CONFIG TBD ON TBD.TBD_TBL_OID = TBL_OID");
        return getGridFromSql(sql.toString());
    }
}