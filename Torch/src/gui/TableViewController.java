package gui;

import java.net.URL;
import java.util.ResourceBundle;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import models.Field;
import models.Table;
import torch.TorchData;

public class TableViewController implements Initializable {
	double orgSceneX, orgSceneY;
	double orgTranslateX, orgTranslateY;
	Table table;
	boolean deleteFieldFlag = false;

	@FXML
	TableColumn<Field, String> tableColumns;
	@FXML
	TableView<Field> tableView;
	@FXML
	AnchorPane anchorPane;
	@FXML
	TitledPane titledPane;

	public void setTitle(String title) {
		titledPane.setText(title);
	}

	@Override
	public void initialize(URL url, ResourceBundle bundle) {
		titledPane.setCollapsible(false);

		titledPane.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				((TitledPane)(event.getSource())).toFront();
				orgSceneX = event.getSceneX();
				orgSceneY = event.getSceneY();
				orgTranslateX = titledPane.getTranslateX();
				orgTranslateY = titledPane.getTranslateY();

				// menu created from the click on the title pane, to add relationships.
				if(event.isSecondaryButtonDown()) {
					ContextMenu menu = GuiUtils.buildRelationshipMenu(table);

					// remove table menuItem
					// This needs to be here instead of the static GuiUtils, when in GuiUtils it will always delete the most recently added table
					MenuItem removeTable = new MenuItem("Remove Table");
					removeTable.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent actEvent) {
							KeyEvent delete = new KeyEvent(null, null, KeyEvent.KEY_PRESSED, null, null, KeyCode.DELETE, false, false, false, false);
							((TitledPane)(event.getSource())).fireEvent(delete);
						}
					});
					menu.getItems().add(removeTable);

					titledPane.setContextMenu(menu);
				}
			}
		});

		titledPane.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				double offsetX = event.getSceneX() - orgSceneX;
				double offsetY = event.getSceneY() - orgSceneY;
				double newTranslateX = orgTranslateX + offsetX;
				double newTranslateY = orgTranslateY + offsetY;
				titledPane.setTranslateX(newTranslateX);
				titledPane.setTranslateY(newTranslateY);
			}
		});

		tableColumns.setCellValueFactory(new PropertyValueFactory<Field, String>("databaseName"));

		tableView.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				orgSceneX = event.getSceneX();
				orgSceneY = event.getSceneY();
				orgTranslateX = titledPane.getTranslateX();
				orgTranslateY = titledPane.getTranslateY();

				if(event.isSecondaryButtonDown()) {
					ContextMenu menu = GuiUtils.buildFieldMenu(table, tableView);
					tableView.setContextMenu(menu);
				}
			}
		});

		tableView.setOnKeyPressed(new EventHandler<KeyEvent>() {
			@Override
			public void handle(KeyEvent event) {
				if(KeyCode.DELETE.equals((event.getCode()))){
					Field selectedField = tableView.getSelectionModel().getSelectedItem();
					if(selectedField != null) {
						deleteFieldFlag = true;
						tableView.getItems().remove(selectedField);
						TorchData.getInstance().removeDisplayedField(selectedField.getFieldId());
					}
					TorchData.getInstance().removeLinesConnectedTo(table.getTableId());
				}
			}
		});
	}

	public void setTable(Table table) {
		this.table = table;
	}
}
