package gui;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.control.TitledPane;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import models.Field;
import models.LineSeries;
import models.Relationship;
import models.Table;
import torch.TorchData;
import torch.TorchViewer;
/**
 * Utilities class for JavaFX specific methods and classes
 */
public class GuiUtils {

	public static Map<String, TableViewController> controllerByTableId = new TreeMap<>();

	public static ContextMenu buildFieldMenu(Table table, TableView<Field> tableView)
	{
		ContextMenu menu = new ContextMenu();
		TorchData tData = TorchData.getInstance();
		for(Field field : table.getFields().values()) {
			// if field is not already displayed
			if(!tData.getDisplayedFields().containsKey(field.getFieldId())) {
				MenuItem item = new MenuItem(field.getDatabaseName());
				item.setMnemonicParsing(false);
				item.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						tableView.getItems().add(field);
						tData.addDisplayedField(field.getFieldId(), field);
					}
				});
				menu.getItems().add(item);
			}
		}
		return menu;
	}

	public static ContextMenu buildRelationshipMenu(Table table)
	{
		TorchData tData = TorchData.getInstance();
		Map<String, Table> tables = tData.getTables();

		ContextMenu menu = new ContextMenu();

		Menu connectTo = new Menu("Connected To");
		Menu connectFrom = new Menu("Connected From");

		// primary relationships
		for(Relationship rel : table.getPrimaryRelationships()) {
			Table t = tables.get(rel.getRelatedTableId());
			if(!tData.isTableDisplayed(t.getTableId())) {
				MenuItem item = null;

				try {
					item = new MenuItem(table.getDatabaseName() + "." + table.getFields().get(rel.getPrimaryFieldId()).getDatabaseName() + " --> " + t.getDatabaseName() + "." + t.getFields().get(rel.getRelatedFieldId()).getDatabaseName());
					// when item is clicked, add the related table to the scene
					item.setOnAction(new EventHandler<ActionEvent>() {
						@Override
						public void handle(ActionEvent event) {
							GuiUtils.addTablePane(t);
						}
					});
				} catch(NullPointerException npe) {
					npe.printStackTrace();
				}
				item.setMnemonicParsing(false);
				connectTo.getItems().add(item);
			} else {
				System.out.println("Related table with id [" + t.getTableId() + "] already displayed.");
			}
		}
		// tables that are related to this table
		for(Relationship rel : table.getRelatedRelationships()) {
			Table t = tables.get(rel.getPrimaryTableId());
			if(!tData.isTableDisplayed(t.getTableId())) {
			MenuItem item = null;
			try {
				item = new MenuItem(table.getDatabaseName() + "." + table.getFields().get(rel.getRelatedFieldId()).getDatabaseName() + " <-- " + t.getDatabaseName() + "." + t.getFields().get(rel.getPrimaryFieldId()).getDatabaseName());
				// when item is clicked, add the related table to the scene
				item.setOnAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						GuiUtils.addTablePane(t);
					}
				});
			} catch(NullPointerException npe) {
				npe.printStackTrace();
			}
			item.setMnemonicParsing(false);
			connectFrom.getItems().add(item);
			} else {
				System.out.println("Related table with id [" + t.getTableId() + "] already displayed.");
			}
		}

		// TODO - if menu has no items, add a dummy "none" entry
		if(connectTo.getItems().size() == 0) {
			MenuItem item = new MenuItem("<None Found>");
			connectTo.getItems().add(item);
		}
		if(connectFrom.getItems().size() == 0) {
			MenuItem item = new MenuItem("<None Found>");
			connectFrom.getItems().add(item);
		}

		menu.getItems().addAll(connectTo);
		menu.getItems().addAll(connectFrom);

		return menu;
	}

	public static void addTablePane(Table table)
	{
		try {
			TorchData tData = TorchData.getInstance();
			if(!tData.getDisplayedTables().containsKey(table.getTableId()))	{
				FXMLLoader tableLoader = new FXMLLoader();
				tableLoader.setLocation(TorchViewer.class.getResource("/gui/TableView.fxml"));
				TitledPane pane = tableLoader.load();
				TableViewController tableViewController = tableLoader.getController();
				tableViewController.setTitle(table.getDatabaseName());
				tableViewController.setTable(table);
				controllerByTableId.put(table.getTableId(), tableViewController);
				tData.getCenterPane().getChildren().add(pane);
				tData.addDisplayedTable(table.getTableId(), table);

				drawConnections(table, pane);

				pane.setOnKeyPressed(new EventHandler<KeyEvent>(){
					@Override
					public void handle(KeyEvent event) {
						if(tableViewController.deleteFieldFlag){
							tableViewController.deleteFieldFlag = false;
							event.consume();
						}
						else if(KeyCode.DELETE.equals(event.getCode())) {
							removeTablePane(table);
							TableView<Field> tableView = tableViewController.tableView;
							if(tableView.getItems() != null) {
								for(Field field : tableView.getItems()){
									TorchData.getInstance().removeDisplayedField(field.getFieldId());
								}
							}
							tData.removeLinesConnectedTo(table.getTableId());
							TorchData.getInstance().getCenterPane().getChildren().remove(pane);
						}
					}
				});
			} else {
				System.out.println("Table already displayed.");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void removeTablePane(Table table) {
		TorchData tData = TorchData.getInstance();
		if(tData.getDisplayedTables().containsKey(table.getTableId())) {
			tData.removeDisplayedTable(table.getTableId());
		} else {
			System.out.println("Table cannot be removed, it is not being displayed.");
		}
	}

	public interface AutoCompleteComparator<T> {
		boolean matches(String typedText, T objectToCompare);
	}

	public static void autoCompleteComboBox(ComboBox<Table> comboBox, AutoCompleteComparator<Table> comparatorMethod) {
		ObservableList<Table> data = comboBox.getItems();
		comboBox.setEditable(true);
		comboBox.getEditor().focusedProperty().addListener(observable -> {
			if (comboBox.getSelectionModel().getSelectedIndex() < 0) {
				comboBox.getEditor().setText(null);
			}
		});

		comboBox.setOnHidden(new EventHandler<Event>() {
			@Override
			public void handle(Event event) {
				comboBox.valueProperty().set(null);
				comboBox.getEditor().setText(null);
				comboBox.setItems(data);
			}
		});

		comboBox.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler<KeyEvent>() {

			private boolean moveCaretToPos = false;
			private int caretPos;

			@Override
			public void handle(KeyEvent event) {
				if (event.getCode() == KeyCode.UP) {
					caretPos = -1;
					moveCaret(getComboBoxText(comboBox).length());
					return;
				} else if (event.getCode() == KeyCode.DOWN) {
					if (!comboBox.isShowing()) {
						comboBox.show();
					}
					caretPos = -1;
					moveCaret(getComboBoxText(comboBox).length());
					return;
				} else if (event.getCode() == KeyCode.BACK_SPACE) {
					moveCaretToPos = true;
					caretPos = comboBox.getEditor().getCaretPosition();
				} else if (event.getCode() == KeyCode.DELETE) {
					moveCaretToPos = true;
					caretPos = comboBox.getEditor().getCaretPosition();
				} else if (event.getCode() == KeyCode.ENTER) {
					return;
				}

				if (event.getCode() == KeyCode.RIGHT || event.getCode() == KeyCode.LEFT || event.getCode().equals(KeyCode.CONTROL)
						|| event.isControlDown() || event.getCode() == KeyCode.HOME	|| event.getCode() == KeyCode.END || event.getCode() == KeyCode.TAB) {
					return;
				}

				ObservableList<Table> list = FXCollections.observableArrayList();
				for (Table aData : data) {
					if (aData != null && comboBox.getEditor().getText() != null && comparatorMethod.matches(comboBox.getEditor().getText(), aData)) {
						list.add(aData);
					}
				}
				String t = getComboBoxText(comboBox);

				comboBox.setItems(list);
				comboBox.getEditor().setText(t);
				if (!moveCaretToPos) {
					caretPos = -1;
				}
				moveCaret(t.length());
				if (!list.isEmpty()) {
					comboBox.show();
				}
			}

			private void moveCaret(int textLength) {
				if (caretPos == -1) {
					comboBox.getEditor().positionCaret(textLength);
				} else {
					comboBox.getEditor().positionCaret(caretPos);
				}
				moveCaretToPos = false;
			}
		});
	}

	public static Table getComboBoxValue(ComboBox<Table> comboBox) {
		if (comboBox.getSelectionModel().getSelectedIndex() < 0) {
			return null;
		} else {
			return comboBox.getItems().get(comboBox.getSelectionModel().getSelectedIndex());
		}
	}

	public static String getComboBoxText(ComboBox<Table> comboBox) {
		if (comboBox == null || comboBox.getEditor() == null || comboBox.getEditor().getText() == null) {
			return "";
		} else {
			return comboBox.getEditor().getText();
		}
	}

	public static TitledPane getPaneForTable(String key) {
		if(controllerByTableId.get(key) != null) {
			return controllerByTableId.get(key).titledPane;
		}
		return null;
	}

	public static void drawConnections(Table table, TitledPane titledPane) {
		TorchData tData = TorchData.getInstance();

		if(table.getPrimaryRelationships() != null) {
			for(Relationship rel : table.getPrimaryRelationships()) {
				if(tData.isTableDisplayed(rel.getRelatedTableId())) {
					TitledPane relPane = getPaneForTable(rel.getRelatedTableId());
					drawTableConnection(table.getTableId(), titledPane, rel.getRelatedTableId(), relPane);
				}
			}
		}

		if(table.getRelatedRelationships() != null) {
			for(Relationship rel : table.getRelatedRelationships()) {
				if(tData.isTableDisplayed(rel.getPrimaryTableId())) {
					TitledPane relPane = getPaneForTable(rel.getPrimaryTableId());
					drawTableConnection(rel.getPrimaryTableId(), relPane, rel.getRelatedTableId(), titledPane);
				}
			}
		}
	}

	private static void drawTableConnection(String startTableId, TitledPane startPane, String endTableId, TitledPane endPane){
		double startX = startPane.getLayoutX() + startPane.getPrefWidth()/2;
		double startY = startPane.getLayoutY() + startPane.getPrefHeight()/2;

		double endX = endPane.getLayoutX() + endPane.getTranslateX() + endPane.getWidth()/2;
		double endY = endPane.getLayoutY() + endPane.getTranslateY() + endPane.getHeight()/2;

		LineSeries<TitledPane> series = new LineSeries<>();
		series.setStartRegion(startPane);
		series.setEndRegion(endPane);
		series.connect(startX, startY, endX, endY, startTableId, endTableId);

		TorchData.getInstance().addLineSeries(series);
	}
}
