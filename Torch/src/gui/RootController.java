package gui;

import java.net.URL;
import java.util.Collection;
import java.util.ResourceBundle;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;
import models.Table;
import torch.TorchData;

public class RootController implements Initializable {
	@FXML
	private ComboBox<Table> tableCombo;
	@FXML
	private Pane centerPane;


	public void setDropDownData(Collection<String> table) {
		tableCombo.getItems().clear();
		tableCombo.getItems().addAll(TorchData.getInstance().getSortedTables().values());
	}

	@Override
	public void initialize(URL url, ResourceBundle bundle) {
		// add the center pain to our singleton
		TorchData tData = TorchData.getInstance();
		tData.setCenterPane(centerPane);
		
		tableCombo.setConverter(new StringConverter<Table>() {
			@Override
			public String toString(Table t) {
				return t == null ? null : t.getDatabaseName();
			}

			@Override
			public Table fromString(String string) {
				TorchData.getInstance().getSortedTables().get(string);
				return null;
			}
		});

		GuiUtils.autoCompleteComboBox(tableCombo, (typedText, itemToCompare) -> itemToCompare.toString().toLowerCase().contains(typedText.toLowerCase()));

		tableCombo.getSelectionModel().selectedItemProperty()
		.addListener(new ChangeListener<Table>() {
			@Override
			public void changed(ObservableValue<? extends Table> table, Table oldValue, Table newValue) {
				if(newValue == null)
				{
					GuiUtils.addTablePane(oldValue);
				}

			}
		});
	}
}
