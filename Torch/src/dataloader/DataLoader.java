package dataloader;

import java.io.InputStream;

public interface DataLoader {

	// TODO - may want to pass a File here and then deal with it in an abstract class?
    void loadData(InputStream stream);

}