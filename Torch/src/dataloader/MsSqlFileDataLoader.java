package dataloader;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import models.Field;
import models.Relationship;
import models.Schema;
import models.Table;
import torch.TorchData;

public class MsSqlFileDataLoader implements DataLoader {

	TorchData torchData = TorchData.getInstance();
	Map<String, Table> tables = torchData.getTables();
	Map<String, Relationship> relationships = torchData.getRelationships();
	Map<String, Schema> schemas = torchData.getSchemas();

	@Override
	public void loadData(InputStream stream) {
		System.out.println("Loading MSSQL data.");
		
		// create default dbo schema
		Schema schema = new Schema();
		schema.setDatabaseName("dbo");
		schema.setSchemaId("dbo");
		schema.setDescriptiveName("Default Schema");
		schemas.put("dbo", schema);

		// read in file data and parse
		Scanner sc = null;
		StringBuffer statement = new StringBuffer(1024);
		try {
			sc = new Scanner(stream, "UTF-16");
			while (sc.hasNextLine()) {
				String line = sc.nextLine() + "\n";
				if(line.startsWith("GO")) {
					parseStatement(statement.toString());
					statement = new StringBuffer(1024);
				} else {
					statement.append(line);
				}
			}
			if (sc.ioException() != null) {
				throw new RuntimeException("Error reading file: " + sc.ioException());
			}
		} finally {
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (sc != null) {
				sc.close();
			}
		}
		System.out.println("Finished loading MSSQL data.");
	}

	private void parseStatement(String statement) {
		statement = removeComments(statement);

		// CREATE SCHEMA
		if(statement.startsWith("CREATE SCHEMA")) {
			parseSchema(statement);
		}
		// CREATE TABLE .. COLUMN NAMES - join to table to schema and columns to table
		if(statement.startsWith("CREATE TABLE")) {
			parseCreateTable(statement);
		}
		// ALTER TABLE ... FOREIGN KEY CONSTRAINT - join to primary schema.table.field and related schema.table.field
		if(statement.startsWith("ALTER TABLE") && statement.contains("FOREIGN KEY")) {
			parseForeignKey(statement);
		}
	}

	private void parseSchema(String statement) {
		statement = statement.replace("CREATE SCHEMA", "");
		StringTokenizer tokenizer = new StringTokenizer(statement, " ");
		String token = removeWrappers(tokenizer.nextToken());
		Schema schema = new Schema();
		schema.setSchemaId(token);
		schema.setDatabaseName(token);
		schemas.put(token, schema);
	}

	public void parseCreateTable(String statement)
	{
		String databaseId = null;
		String schemaId = null;
		String tableId = null;

		// strip out unnecessary options with regex
		// remove CONSTRAINT section
		statement = statement.replaceAll("CONSTRAINT[_A-Za-z0-9\\s\\[\\]\\(]+[)]{1}", "");
		// remove WITH section
		statement = statement.replaceAll("WITH[_A-Za-z\\s(=,]+[)]{1}", "");
		//  find table name: CREATE TABLE ([A-Za-z0-9\[\]\.]+)[(]{1} 

		String tableNamePattern = "CREATE TABLE ([A-Za-z0-9\\[\\]\\.]+)[(]{1}";
		Pattern pattern = Pattern.compile(tableNamePattern);
		Matcher matcher = pattern.matcher(statement);
		String tableLine = null;
		if(matcher.find()) {
			// table name in group one.
			tableLine = matcher.group(1);
		}
		// tokenize on dot, 1 token = table, 2 tokens = schema.table, 3 tokens = database.schema.table
		StringTokenizer tableLineTokenizer = new StringTokenizer(tableLine, ".");
		if(tableLineTokenizer.countTokens() == 1) {
			tableId = tableLineTokenizer.nextToken(); 
		}
		else if(tableLineTokenizer.countTokens() == 2) {
			schemaId = tableLineTokenizer.nextToken();
			tableId = tableLineTokenizer.nextToken(); 
		}
		else if(tableLineTokenizer.countTokens() == 3) {
			databaseId = tableLineTokenizer.nextToken();
			schemaId = tableLineTokenizer.nextToken();
			tableId = tableLineTokenizer.nextToken(); 
		}

		// remove wrappers
		databaseId = removeWrappers(databaseId);
		schemaId = removeWrappers(schemaId);
		tableId = schemaId + "." + removeWrappers(tableId);

		// create table, add to schema tables
		Table table = new Table();
		table.setDatabaseName(tableId);
		table.setTableId(tableId);
		table.setSchemaId(schemaId);
		tables.put(table.getTableId(), table);
		schemas.get(schemaId).addTable(table.getTableId(), table);
		
		// parse the columns for this table
		parseFields(statement, table);
	}

	public void parseFields(String statement, Table table)
	{
		// find table entries, each entry found in group 1
		final String regex = "^[\\s\\[]([\\[A-Za-z0-9\\]\\s()]+[,]?[A-Za-z0-9)\\s]+),";
		final Pattern pattern = Pattern.compile(regex, Pattern.MULTILINE);
		final Matcher matcher = pattern.matcher(statement);

		while (matcher.find()) {
			String columnLine = matcher.group(1);
			// tokenize on space, first is column name, second is type, possibly followed by (length)
			StringTokenizer tokenizer = new StringTokenizer(columnLine, "[]");
			int tokenIndex = 1;
			String columnName = null;
			String databaseType = null;
			String length = null;
			while(tokenizer.hasMoreTokens()) {
				String token = tokenizer.nextToken().trim();
				if(tokenIndex == 1) {
					columnName = token;
				}
				if(tokenIndex == 3) {
					databaseType = token;
				}
				if(tokenIndex == 4)	{
					if(token.startsWith("(")) {
						char[] tokenArray = token.toCharArray();
						for(char c : tokenArray)
						{
							if(c == '(') 
								continue;
							if(c == ')') 
								break;
							if(length == null) 
									length = "";

							length = length + String.valueOf(c);
						}
					}
				}
				tokenIndex++;
			}
			// create field, add to fields list for table
			Field field = new Field();
			field.setFieldId(table.getTableId() + "." + columnName);
			field.setTableId(table.getTableId());
			field.setDatabaseName(columnName);
			field.setDatabaseType(databaseType);
			if(length != null && isInt(length)) {
				field.setDatabaseLength(Integer.valueOf(length));
			}
			tables.get(table.getTableId()).addField(field.getFieldId(), field);
		}
	}
	
	private void parseForeignKey(String statement) {

		String relationshipId = null;
		String primaryDatabaseId = null;
		String primaryTableId = null;
		String primarySchemaId = null;
		String primaryFieldId = null;

		String relatedDatabaseId = null;
		String relatedTableId = null;
		String relatedSchemaId = null;
		String relatedFieldId = null;

		// get relationship Id
		final String relIdRegex = "CONSTRAINT [\\[]([A-Za-z0-9_]+)[\\]]";
		final Pattern relIdPattern = Pattern.compile(relIdRegex, Pattern.MULTILINE);
		final Matcher relIdMatcher = relIdPattern.matcher(statement);
		if(relIdMatcher.find()) {
			if(relIdMatcher.groupCount() > 0) {
				relationshipId = relIdMatcher.group(1);
			}
		}

		// get primary database, schema, table, field
		String primaryTableStmt = null;
		String primaryFieldStmt = null;
		final String primaryTableRegex = "ALTER TABLE ([\\[A-Za-z0-9_.\\]]+)";
		final Pattern primaryTablePattern = Pattern.compile(primaryTableRegex, Pattern.MULTILINE);
		final Matcher primaryTableMatcher = primaryTablePattern.matcher(statement);
		if(primaryTableMatcher.find()) {
			if(primaryTableMatcher.groupCount() > 0) {
				primaryTableStmt = primaryTableMatcher.group(1);
			}
		}

		final String primaryFieldRegex = "FOREIGN KEY[\\s(]([\\[A-Za-z0-9_.\\]]+)";
		final Pattern primaryFieldPattern = Pattern.compile(primaryFieldRegex, Pattern.MULTILINE);
		final Matcher primaryFieldMatcher = primaryFieldPattern.matcher(statement);
		if(primaryFieldMatcher.find()) {
			if(primaryFieldMatcher.groupCount() > 0) {
				primaryFieldStmt = primaryFieldMatcher.group(1);
			}
		}
		String primaryRelationship = primaryTableStmt + "." + primaryFieldStmt;
		
		// tokenize on dot, 1 token = table, 2 tokens = schema.table, 3 tokens = database.schema.table
		StringTokenizer primaryRelationshipTokenizer = new StringTokenizer(primaryRelationship, ".");
		if(primaryRelationshipTokenizer.countTokens() == 3) {
			primarySchemaId = primaryRelationshipTokenizer.nextToken();
			primaryTableId = primaryRelationshipTokenizer.nextToken();
			primaryFieldId = primaryRelationshipTokenizer.nextToken();
		}
		if(primaryRelationshipTokenizer.countTokens() == 4) {
			primaryDatabaseId = primaryRelationshipTokenizer.nextToken();
			primarySchemaId = primaryRelationshipTokenizer.nextToken();
			primaryTableId = primaryRelationshipTokenizer.nextToken();
			primaryFieldId = primaryRelationshipTokenizer.nextToken();
		}

		// get related database, schema, table, field
		String relTableStmt = null;
		String relFieldStmt = null;
		final String relTableRegex = "REFERENCES[\\s(]([\\[A-Za-z0-9_.\\]]+)";
		final Pattern relTablePattern = Pattern.compile(relTableRegex, Pattern.MULTILINE);
		final Matcher relTableMatcher = relTablePattern.matcher(statement);
		if(relTableMatcher.find()) {
			if(relTableMatcher.groupCount() > 0) {
				relTableStmt = relTableMatcher.group(1);
			}
		}

		final String relFieldRegex = "REFERENCES[\\s(][\\[A-Za-z0-9_.\\]\\s]+[\\(]([\\[A-Za-z0-9\\]]+)";
		final Pattern relFieldPattern = Pattern.compile(relFieldRegex, Pattern.MULTILINE);
		final Matcher relFieldMatcher = relFieldPattern.matcher(statement);
		if(relFieldMatcher.find()) {
			if(relFieldMatcher.groupCount() > 0) {
				relFieldStmt = relFieldMatcher.group(1);
			}
		}
		String relatedRelationship = relTableStmt + "." + relFieldStmt;
		
		// tokenize on dot, 1 token = table, 2 tokens = schema.table, 3 tokens = database.schema.table
		StringTokenizer relatedRelationshipTokenizer = new StringTokenizer(relatedRelationship, ".");
		if(relatedRelationshipTokenizer.countTokens() == 3) {
			relatedSchemaId = relatedRelationshipTokenizer.nextToken();
			relatedTableId = relatedRelationshipTokenizer.nextToken();
			relatedFieldId = relatedRelationshipTokenizer.nextToken();
		}
		if(relatedRelationshipTokenizer.countTokens() == 4) {
			relatedDatabaseId = relatedRelationshipTokenizer.nextToken();
			relatedSchemaId = relatedRelationshipTokenizer.nextToken();
			relatedTableId = relatedRelationshipTokenizer.nextToken();
			relatedFieldId = relatedRelationshipTokenizer.nextToken();
		}
		
		// remove wrappers
		primaryDatabaseId = removeWrappers(primaryDatabaseId);
		primarySchemaId = removeWrappers(primarySchemaId);
		primaryTableId = removeWrappers(primaryTableId);
		primaryFieldId = removeWrappers(primaryFieldId);
		
		relatedDatabaseId = removeWrappers(relatedDatabaseId);
		relatedSchemaId = removeWrappers(relatedSchemaId);
		relatedTableId = removeWrappers(relatedTableId);
		relatedFieldId = removeWrappers(relatedFieldId);

		// build compound keys to prevent duplication
		primaryTableId = primarySchemaId + "." + primaryTableId;
		primaryFieldId = primaryTableId + "." + primaryFieldId;
		relatedTableId = relatedSchemaId + "." + relatedTableId;
		relatedFieldId = relatedTableId + "." + relatedFieldId;
		
		// create relationship
		if(relationshipId == null || relationshipId.equals(""))
		{
			relationshipId = "FK_" + primaryTableId + "_" + primaryFieldId + "_" + relatedTableId + "_" + relatedFieldId;
		}
		
		Relationship rel = new Relationship();
		rel.setRelationshipId(relationshipId);
		rel.setPrimaryDatabaseId(primaryDatabaseId);
		rel.setPrimarySchemaId(primarySchemaId);
		rel.setPrimaryTableId(primaryTableId);
		rel.setPrimaryFieldId(primaryFieldId);
		rel.setRelatedDatabaseId(relatedDatabaseId);
		rel.setRelatedSchemaId(relatedSchemaId);
		rel.setRelatedTableId(relatedTableId);
		rel.setRelatedFieldId(relatedFieldId);
		relationships.put(relationshipId, rel);
		
//		System.out.println("REL ID: " + relationshipId);
//		System.out.println("PRIM DB: " + primaryDatabaseId);
//		System.out.println("PRIM SCHEMA: " + primarySchemaId);
//		System.out.println("PRIM TABLE: " + primaryTableId);
//		System.out.println("PRIM FIELD: " + primaryFieldId);
//		System.out.println("REL DB: " + relatedDatabaseId);
//		System.out.println("REL SCHEMA: " + relatedSchemaId);
//		System.out.println("REL TABLE: " + relatedTableId);
//		System.out.println("REL FIELD: " + relatedFieldId);
		
		// add primary relationships to tables and fields
		if(tables.get(primaryTableId) != null)
		{
			tables.get(primaryTableId).addRelationship(rel);
			if(tables.get(primaryTableId).getFields().get(primaryFieldId) != null)
			{
				tables.get(primaryTableId).getFields().get(primaryFieldId).addPrimaryRelationship(rel);
			}
		}
		// add related relationships to tables and fields
		if(tables.get(relatedTableId) != null)
		{
			tables.get(relatedTableId).addRelatedRelationship(rel);
			if(tables.get(relatedTableId).getFields().get(relatedFieldId) != null)
			{
				tables.get(relatedTableId).getFields().get(relatedFieldId).addRelatedRelationship(rel);
			}
		}
		
	}

	/**
	 * Remove [] wrappers from a string
	 */
	private static String removeWrappers(String string) {
		if(string != null && string.length() > 1) {
			if(string.substring(0, 1).equals("[") || string.substring(string.length()-1, string.length()).equals("]")) {
				string = string.substring(1, string.length()-1);
			}
		}
		return string;
	}
	
	/**
	 * Remove these comments <forward slash>***** comments *****<forward slash>
	 */ 
	private static String removeComments(String statement) {
		if(statement != null) {
			statement = statement.replaceAll("/\\*(?:.|[\\n\\r])*?\\*/","");
			statement = statement.trim();
		}
		return statement;
	}
	
	/**
	 * Returns a boolean indicating whether this string is a number or not
	 * @param s - The string to test for being a number
	 * @return - a boolean indicating whether this string is a number or not.
	 */
	private static boolean isInt(String s)
	{
	 for(int a=0;a<s.length();a++)
	 {
	    if(a==0 && s.charAt(a) == '-') continue;
	    if( !Character.isDigit(s.charAt(a)) ) return false;
	 }
	 return true;
	}
}
