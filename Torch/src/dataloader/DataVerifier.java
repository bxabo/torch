package dataloader;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import models.Field;
import models.Relationship;
import models.Table;
import torch.TorchData;

public class DataVerifier {

	private static Map<String, Table> tables;
	private static Map<String, Relationship> allRelationships;

	private static int tableErrorCount = 0;
	private static int fieldErrorCount = 0;
	private static int relationshipErrorCount = 0;

	/**
	 * Verifies the data and linking between the tables, fields, and relationships
	 */
	public static void verify() {
		System.out.println("Verifying Data.");
		TorchData tData = TorchData.getInstance();
		tables = tData.getTables();
		allRelationships = new HashMap<>(tData.getRelationships());

		verifyTables();
		verifyFields();
		verifyRelationships();

		System.out.println("Finished verifying data.");
		if(tableErrorCount + fieldErrorCount + relationshipErrorCount > 0)
		{
			System.out.println("Data errors were found: ");
			System.out.println("Table errors found: " + tableErrorCount);
			System.out.println("Field errors found: " + fieldErrorCount);
			System.out.println("Relationship errors found: " + relationshipErrorCount);
		} else {
			System.out.println("Success! No errors found!");
		}
	}

	/**
	 * Loop through the tables and check for data issues.
	 */
	private static void verifyTables()	{
		// loop tables, look for issues
		int tableCount = 0;
		for(Map.Entry<String, Table> entry: tables.entrySet()) {
			// should be no empty keys
			String key = entry.getKey();
			Table t = entry.getValue();
			if(key == null || key.equals("")) 
				logError("Missing key for table: " + t, t);
			if(isEmpty(t.getTableId()))
				logError("Missing table ID for table: " + t, t);
			if(isEmpty(t.getDatabaseName()))
				logError("Missing database name for table: " + t, t);
			if(t.getFields() == null || t.getFields().size() == 0)
				logError("Missing fields for table: " + t, t);
			tableCount++;
		}
		System.out.println("Checked " + tableCount + " table(s).");

	}

	/**
	 * Loop through the fields and check for data issues.
	 */
	private static void verifyFields()	{
		// loop fields, look for issues
		int fieldCount = 0;
		for(Map.Entry<String, Table> tableEntry: tables.entrySet()) {
			for(Map.Entry<String, Field> fieldEntry : tableEntry.getValue().getFields().entrySet())	{
				// should be no empty keys
				String key = fieldEntry.getKey();
				Field f = fieldEntry.getValue();
				if(key == null || key.equals(""))
					logError("Missing key for field: " + f, f);
				if(isEmpty(f.getFieldId()))
					logError("Missing field ID for field: " + f, f);
				if(isEmpty(f.getDatabaseName()))
					logError("Missing database name for field: " + f, f);
				fieldCount++;
			}
		}

		System.out.println("Checked " + fieldCount + " field(s).");
	}

	/**
	 * Loop through relationships and make sure the source and destination table and field
	 * actually relate to a Table and Field object by the identifier given in the source data.
	 * 
	 * It is possible that the identifier used on the relationship does not match the ID of the
	 * field.  This should not be the case, but there may be errors in the source data.  If 
	 * these identifiers do not relate properly the GUI interface will fail to function correctly,
	 * potentially throw exceptions when trying to build parts of the schema when relationships
	 * are looked up.
	 */
	private static void verifyRelationships() {
		int relationshipCount = 0;
		for(Map.Entry<String, Table> tableEntry : tables.entrySet()) {

			// relationships - connected from and connected to - this table
			Collection<Relationship> relationships = new ArrayList<>();
			relationships.addAll(tableEntry.getValue().getPrimaryRelationships());
			relationships.addAll(tableEntry.getValue().getRelatedRelationships());

			for(Relationship rel : tableEntry.getValue().getPrimaryRelationships()) {

				String primaryFieldId = rel.getPrimaryFieldId();
				String relatedFieldId = rel.getRelatedFieldId();
				String primaryTableId = rel.getPrimaryTableId();
				String relatedTableId = rel.getRelatedTableId();
				String relationshipId = rel.getRelationshipId();

				if(isEmpty(relationshipId))
					logError("Missing relationship ID for relationship: " + rel, rel);
				if(isEmpty(primaryFieldId))
					logError("Missing primary field ID for relationship: " + rel, rel);
				if(isEmpty(relatedFieldId))
					logError("Missing related field ID for relationship: " + rel, rel);
				if(isEmpty(primaryTableId))
					logError("Missing primary table ID for relationship: " + rel, rel);
				if(isEmpty(relatedTableId))
					logError("Missing related table ID for relationship: " + rel, rel);				

				Table primaryTable = tables.get(primaryTableId);
				Table relatedTable = tables.get(relatedTableId);
				// check primary table with primary field
				if(primaryTable == null) {
					logError("No primary table found for relationship: " + rel, rel);
				} else {
					if(primaryTable.getFields().get(primaryFieldId) == null) {
						logError("No field ID [" + primaryFieldId + "] found in table [" + primaryTable.getTableId() + "] for relationship: " + rel, rel);
						System.out.println("Possible existing matches: " + getPotentialFields(primaryTable, primaryFieldId));
					}
				}

				// check related table and related field
				if(relatedTable == null) {
					logError("No related table found for relationship: " + rel, rel);
				} else {
					if(relatedTable.getFields().get(relatedFieldId) == null) {
						logError("No related field ID [" + relatedFieldId + "] found in table [" + relatedTable.getTableId() + "] for relationship: " + rel, rel);
						System.out.println("Possible existing matches: " + getPotentialFields(relatedTable, relatedFieldId));
					}
				}

				// remove from relationships, so only the unused will remain
				if(!isEmpty(rel.getPrimaryFieldId())) {
					allRelationships.remove(rel.getRelationshipId());
				}
				relationshipCount++;
			}
		}

		// display potentially unused relationships
		for(Relationship rel : allRelationships.values()) {
			logError("No table related to the loaded relationship: " + rel, rel);
		}

		System.out.println("Checked " + relationshipCount + " relationship(s).");
	}

	/**
	 * Logs an error to the standard out and increments a counter correlated to the
	 * type of object the error applies to.
	 * 
	 * @param message
	 * @param o
	 */
	private static void logError(String message, Object o)	{
		System.out.println("Data Error: " + message);
		if(o instanceof Table)
			tableErrorCount++;
		if(o instanceof Field)
			fieldErrorCount++;
		if(o instanceof Relationship)
			relationshipErrorCount++;
	}

	/**
	 * Given a table and a fieldId that does not exist in that table, attempt to find likely matches to that field.
	 * @param table - Table object to search for a potential field ID match.
	 * @param fieldId - The String that contains the missing field ID that may have an existing match in the table.
	 */
	private static List<String> getPotentialFields(Table table, String fieldId) {
		List<String> matches = new ArrayList<>();
		Map<String, Field> fields = table.getFields();
		String matchText = null;

		if(fieldId.length() > 6) {
			// TODO - a matching regex should be set in a config potentially so that this is more generic
			matchText = fieldId.substring(0, 10);
		}

		// if we don't have text we can match on, don't bother looking
		if(matchText == null) return null;

		/* look through the fields and if the fieldId of this field contains the match text
		 * from the passed fieldId that was not found, add it to the potential field list.
		 */
		for(Field field : fields.values()) {
			if(field.getFieldId().contains(matchText)) {
				matches.add(field.getFieldId());
			}
		}
		return matches;
	}

	/**
	 * Checks the given value for null or empty
	 */
	private static boolean isEmpty(Object value) {
		if(value == null)
			return true;
		if(value.equals(""))
			return true;
		return false;
	}
}
