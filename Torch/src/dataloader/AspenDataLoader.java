package dataloader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import models.Field;
import models.Relationship;
import models.Schema;
import models.Table;
import torch.TorchData;

public class AspenDataLoader implements DataLoader {
	HashMap<String, String> replacements;

	public void loadData(InputStream inputStream) {		
		initializeReplacements();

		TorchData torchData = TorchData.getInstance();
		Map<String, Table> tables = torchData.getTables();
		Map<String, Relationship> relationships = torchData.getRelationships();
		Map<String, Schema> schemas = torchData.getSchemas();

		parseSchemas(schemas);
		
		ZipInputStream zipStream = (ZipInputStream) inputStream;

		BufferedReader reader = new BufferedReader(new InputStreamReader(zipStream));
		ZipEntry zipEntry;
		try {
			while((zipEntry = zipStream.getNextEntry()) != null) {
				if(zipEntry.getName() != null) {
					if(zipEntry.getName().toLowerCase().contains("tables")) {
						parseTables(reader, tables, schemas);
					}
					else if(zipEntry.getName().toLowerCase().contains("fields")) {
						parseFields(reader, tables);
					}
					else if(zipEntry.getName().toLowerCase().contains("relationships")) {
						parseRelationships(reader, tables, relationships);
					}
					else {
						System.out.println("Unknown file in zip archive: " + zipEntry.getName());
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void parseSchemas(Map<String, Schema> schemas) {
		Schema schema = new Schema();
		schema.setSchemaId("default");
		schema.setDatabaseName("");
		schema.setDescriptiveName("Default Schema");
		schemas.put(schema.getSchemaId(), schema);
	}

	public void parseTables(BufferedReader reader, Map<String, Table> tables, Map<String, Schema> schemas) {
		Map<Integer, List<String>> rows = parseRows(reader);
		String schemaId = "default";
		for(Map.Entry<Integer, List<String>> entry : rows.entrySet()) {
			Schema schema = schemas.get(schemaId);
			Table table = new Table();
			List<String> columnValues = entry.getValue();
			// TODO : this could be better - skip 3 header and 1 footer rows in the data file(s)
			if(entry.getKey() < 3 || entry.getKey() == rows.size() - 1) 
				continue;
			table.setTableId(columnValues.get(1));
			table.setClassName(columnValues.get(2));
			table.setDatabaseName(columnValues.get(3));
			table.setPrefix(columnValues.get(4));
			table.setDescriptiveName(columnValues.get(5));
			table.setCodebase(columnValues.get(7));
			tables.put(table.getTableId(), table);
			schema.addTable(table.getTableId(), table);
		}
	}

	public void parseFields(BufferedReader reader, Map<String, Table> tables) {
		Map<Integer, List<String>> rows = parseRows(reader);
		ArrayList<String> ignoredFields = new ArrayList<>();
		ignoredFields.add("fieldA");
		ignoredFields.add("fieldB");
		ignoredFields.add("fieldC");
		ignoredFields.add("fieldD");
		ignoredFields.add("fieldE");

		for(Map.Entry<Integer, List<String>> entry : rows.entrySet()) {
			Field field = new Field();
			List<String> columnValues = entry.getValue();
			// TODO : this could be better - skip 3 header and 1 footer rows in the data file(s)
			if(entry.getKey() < 3 || entry.getKey() == rows.size() - 1) 
				continue;

			/* TODO : skip user defined fields for now, the are stored in a count that
			 * needs to be unwrapped and have dubious value in this application
			 */
			if(ignoredFields.contains(columnValues.get(7)))
				continue;

			String tableId = columnValues.get(1);
			field.setTableId(tableId);
			String fieldId = columnValues.get(2);
			/* XXX - fix for field IDs that do not match their relationship ID value
			 * This is usually resolved by removing the OID section, for instance
			 * the field in the fields export may be psnAdrOIDMail, while the relationship
			 * is labeled as psnAdrMail.  Removing OID should allow them to match.
			 */
			if(!fieldId.endsWith("1") && !fieldId.endsWith("2") && !fieldId.endsWith("3") && !fieldId.endsWith("4") && !fieldId.endsWith("5"))
			{
				if(fieldId.matches("[A-Za-z0-9]+OID[A-Za-z0-9]+"))
				{
					fieldId = fieldId.replace("OID", "");
				}
			}

			field.setFieldId(fieldId);
			field.setFieldCount(Integer.valueOf(columnValues.get(3)));
			field.setJdbcType(columnValues.get(4));
			field.setDatabaseLength(Integer.valueOf(columnValues.get(5)));
			try {
				field.setJavaType(Class.forName(columnValues.get(6)));
			} catch (ClassNotFoundException e) {
				// couldn't find class name, set it to null
				field.setJavaType(null);
			}
			field.setJavaName(columnValues.get(7));
			field.setDatabaseName(columnValues.get(8));
			tables.get(tableId).addField(fieldId, field);
		}
	}

	public void parseRelationships(BufferedReader reader, Map<String, Table> tables, Map<String, Relationship> relationships) {
		Map<Integer, List<String>> rows = parseRows(reader);

		for(Map.Entry<Integer, List<String>> entry : rows.entrySet()) {
			Relationship relationship = new Relationship();
			List<String> columnValues = entry.getValue();
			// TODO : this could be better - skip 3 header and 1 footer rows in the data file(s)
			if(entry.getKey() < 3 || entry.getKey() == rows.size() - 1) 
				continue;

			String relationshipId = columnValues.get(1);
			String javaName = columnValues.get(8);
			String tableId = null;
			String fieldId = null;
			String relatedTableId = null;
			String relatedFieldId = null;

			if(!"N".equals(columnValues.get(3)) && !"0".equals(columnValues.get(3)))
			{
				continue;
			}

			tableId = columnValues.get(2);
			relatedTableId = columnValues.get(4);
			fieldId = columnValues.get(6);
			relatedFieldId = columnValues.get(7);
			if(tableId != null && fieldId != null && relatedTableId != null &&  relatedFieldId != null)
			{
				fieldId = fieldId.replaceAll("idx", "");
				fieldId = fieldId.substring(0, 1).toLowerCase() + fieldId.substring(1, fieldId.length());

				relatedFieldId = relatedFieldId.replaceAll("idx", "");
				relatedFieldId = relatedFieldId.substring(0, 1).toLowerCase() + relatedFieldId.substring(1, relatedFieldId.length());

				// XXX - ugly fixes for mislabeled data in Aspen's data dictionary
				if(relationshipId.equals("relZfsZfmOid")) continue;
				fieldId = replaceFieldId(fieldId);
				relatedFieldId = replaceFieldId(relatedFieldId);
				// XXX - end ugly fixes
				
				relationship.setRelationshipId(relationshipId);
				relationship.setPrimaryTableId(tableId);
				relationship.setRelatedTableId(relatedTableId);
				relationship.setPrimaryFieldId(fieldId);
				relationship.setRelatedFieldId(relatedFieldId);
				relationship.setJavaName(javaName);

				relationships.put(relationshipId, relationship);
				// add relationships to tables and fields
				if(tables.get(tableId) != null)
				{
					tables.get(tableId).addRelationship(relationship);
					if(tables.get(tableId).getFields().get(fieldId) != null)
					{
						tables.get(tableId).getFields().get(fieldId).addPrimaryRelationship(relationship);
					}
				}
				// add related relationships to tables and fields
				if(tables.get(relatedTableId) != null)
				{
					tables.get(relatedTableId).addRelatedRelationship(relationship);
					if(tables.get(relatedTableId).getFields().get(relatedFieldId) != null)
					{
						tables.get(relatedTableId).getFields().get(relatedFieldId).addRelatedRelationship(relationship);
					}
				}
			}
		}
	}

	private Map<Integer, List<String>> parseRows(BufferedReader reader) {
		Map<Integer, List<String>> map = new HashMap<>();
		String line = null;
		int count = 0;
		try {
			while((line = reader.readLine()) != null) {
				StringTokenizer tokenizer = new StringTokenizer(line, "|", false);
				List<String> row = new ArrayList<>();
				while(tokenizer.hasMoreTokens()) {
					String token = tokenizer.nextToken();
					if(token != null){
						row.add(token.trim());
					}
				}
				map.put(count++, row);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}

	private String replaceFieldId(String fieldId) {
		if(replacements.get(fieldId) != null) {
			fieldId = replacements.get(fieldId);
		}
		return fieldId;
	}

	/**
	 * Build map of original value and replacement value.  We are replacing the field IDs from
	 * the relationship input with the correct, matching value in the field input.
	 * 
	 * This is a bit ugly, because there are so many mismatches, field IDs have also been modified
	 * from the source data in parseFields() to result in LESS replacements.  However, that means 
	 * one must take into account that the field ID in relationship needs to match to the ID from
	 * the field input file that has already been modified ("OID" has been stripped out if it is 
	 * followed by a string value, as there are many cases where it is removed from the relationhip
	 * ID.
	 * 
	 * Example: the field ID may original be sabStfOIDSub, however the relationship is 
	 * idxSabStfSubst
	 * 
	 * First we load tables, then fields and strip the "OID", resulting in a field ID of "sabStfSub"
	 * 
	 * Then we load relationships, remove the "idx" and lower case the first letter, leaving 
	 * "sabStfSubst".
	 * 
	 * Since we have to do those steps for every field, most of them work, the follow cases are 
	 * cases that require manual matching, so for this example we map "sabStfSubst" to "sabStfSub"
	 * 
	 *  This is a manual clean up for when the mass replace/modification doesn't work.
	 */
	private void initializeReplacements() {
		replacements = new HashMap<>();
		replacements.put("aptPsnApprv", "aptPsnApt");
		replacements.put("blmRmsPrim", "blmRmsOID");
		replacements.put("fldOID","fldOid");
		replacements.put("emlOID","emlOid");
		replacements.put("gpoOID","gpoOid");
		replacements.put("fldOID","fldFldOid");
		replacements.put("blmStfPrim","blmStfOID");
		replacements.put("cflUsrOID","cflUsrOid");
		replacements.put("cfrCfrOIDPa","cfrCfrPrnt");
		replacements.put("cfrCfrOIDRo","cfrCfrRoot");
		replacements.put("cgdUsrOID","cgdUsrOid");
		replacements.put("crsCrsOIDPa","crsCrsParen");
		replacements.put("cssCSKNext","cssCskNext");
		replacements.put("cjcCjoOIDDe","cjcCjoDebit");
		replacements.put("cmpCdsOIDLe","cmpCdsLessn");
		replacements.put("cmpCdsTo","cmpCdsTopic");
		replacements.put("fldTblOID","fldFldTblOid");
		replacements.put("relIdxPrim", "relIDxPrim");
		replacements.put("relTlbRel","relIdxRel");
		replacements.put("darUsrOIDRe","darUsrRes");
		replacements.put("eatEmlOID","eatEmlOid");
		replacements.put("eatFilOID","eatFilOid");
		replacements.put("emlUsrOID","emlUsrOid");
		replacements.put("serTrfOIDCo","serTrfCom");
		replacements.put("serTrfOIDOp","serTrfOpt");
		replacements.put("serTrfOIDSy","serTrfSyl");
		replacements.put("serTrfOIDLi", "serTrfOpc");
		replacements.put("efdDdxOID","efdDdxOid");
		replacements.put("filUsrOIDCr","filUsrCreat");
		replacements.put("filUsrOIDMo","filUsrModif");
		replacements.put("fmdTbdOIDSt","fmdTbdStore");
		replacements.put("fmdTbdOwner","fmdTbdOwn");
		replacements.put("fpoEditUsrO","fpoEditUsrOID");
		replacements.put("fpoReplyToO","fpoReplyToOID");
		replacements.put("gcdRbdOID","gcdRbaOID");
		replacements.put("gbgGsgMail","gbgGsgMax");
		replacements.put("gbgGsgOIDMi","gbgGsgMin");
		replacements.put("grqGprSub","grqGprSubPg");
		replacements.put("gcaMstOID","gcaMstOid");
		replacements.put("grpUsrOIDCr","grpUsrOID");
		replacements.put("hlgStfRefer","hlgStfOidRef");
		replacements.put("hlgStfResp","hlgStfOidResp");
		replacements.put("ipsIplOID","ipsIepOID");
		replacements.put("lrpCfrOIDRo","lrpCfrOID");
		replacements.put("oatUsrOID","oatUsrOid");
		replacements.put("orgCtxOIDCu","orgCtxCurr");
		replacements.put("orgCtxOIDWa","orgCtxWait");
		replacements.put("orgOrgOIDPa","orgOrgParen");
		replacements.put("orgStfAdm1","orgStfOIDAdm1");
		replacements.put("orgStfAdm2","orgStfOIDAdm2");
		replacements.put("orgStfAdm3","orgStfOIDAdm3");
		replacements.put("orgStfAdm4","orgStfOIDAdm4");
		replacements.put("orgStfAdm5","orgStfOIDAdm5");
		replacements.put("mstRmsPrim","mstRmsOID");
		replacements.put("mstStfPrim","mstStfOID");
		replacements.put("mtxDayOID","mtxDay");
		replacements.put("mtxPerOID","mtxPeriod");
		replacements.put("sodSrdOID","sodSrdOid");
		replacements.put("sodTblOID","sodTblOid");
		replacements.put("sroSodOID","sroSrlOID");
		replacements.put("sklCtxOIDCu","sklCtxCurr");
		replacements.put("sklGtdOIDDe","sklGtdDeflt");
		replacements.put("sklSkxOIDAc","sklSkxActiv");
		replacements.put("sklStfAdm1","sklStfOIDAdm1");
		replacements.put("sklStfAdm2","sklStfOIDAdm2");
		replacements.put("sklStfAdm3","sklStfOIDAdm3");
		replacements.put("sklStfAdm4","sklStfOIDAdm4");
		replacements.put("sklStfAdm5","sklStfOIDAdm5");
		replacements.put("sueTbxOID","sueTdxOID");
		replacements.put("skaStfAdm1","skaStfOIDAdm1");
		replacements.put("skaStfAdm2","skaStfOIDAdm2");
		replacements.put("skaStfAdm3","skaStfOIDAdm3");
		replacements.put("skaStfAdm4","skaStfOIDAdm4");
		replacements.put("skaStfAdm5","skaStfOIDAdm5");
		replacements.put("sflStfSubst","sflStfSub");
		replacements.put("scpStcOIDRe","scpStcRelat");
		replacements.put("reqCskAlt01","reqCskOIDAlt01");
		replacements.put("reqCskAlt02","reqCskOIDAlt02");
		replacements.put("cndStdOIDVc","cndStdVctim");
		replacements.put("cndStfOwner","cndStfOwn");
		replacements.put("sctSccOID","sctSscOID");
		replacements.put("sccMstExg01","sccMstOIDExg01");
		replacements.put("sccMstExg02","sccMstOIDExg02");
		replacements.put("ssrStdrOID","ssrStdROID");
		replacements.put("trnCskEquv","trnCskEqv");
		replacements.put("trnSklOIDSt","trnSklOidStd");
		replacements.put("sabStfSubst","sabStfSub");
		replacements.put("sfxSklOID","sfxSklOid");
		replacements.put("stdCtjOID1","stdContact1");
		replacements.put("stdCtjOID2","stdContact2");
		replacements.put("stdCtjOID3","stdContact3");
		replacements.put("stdSklSummr","stdSklSumr");
		replacements.put("wphWpoOIDSt","wphWpoStd");
		replacements.put("wfdTbdOIDSe","wfdTbdSel");
		replacements.put("wfdTbdOwner","wfdTbdOwn");
		replacements.put("wfdFddOwner", "wfdPhaseOwner");
	}
}
