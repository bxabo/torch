package models;

import java.util.LinkedList;
import java.util.function.Consumer;

import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.layout.Region;
import javafx.scene.shape.Line;

public class LineSeries<T extends Region> {

	private LinkedList<Line> lines;

	private String startTableId, endTableId;
	private T startRegion, endRegion;

	public LineSeries() {
		lines = new LinkedList<>();
	}

	public LinkedList<Line> lines() {
		return lines;
	}

	public void connect(double startX, double startY, double endX, double endY, String startId, String endId) {
		setStartTableId(startId);
		setEndTableId(endId);

		Line line = buildLine(startX, startY, startX, endY);
		Line line2 = buildLine(startX, endY, endX, endY);

		lines.add(line);
		lines.add(line2);
		createListeners();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Line buildLine(double startX, double startY, double endX, double endY) {
		Line line = new Line(startX, startY, endX, endY);
		line.setStrokeWidth(2);
		line.setOnMouseEntered(new EventHandler() {
			@Override public void handle(Event event) {
				lines.forEach(new Consumer<Line>() {
					@Override public void accept(Line lineFor) {
						lineFor.setStyle("-fx-effect: dropShadow(one-pass-box, #039ed3, 6, 1.0, 0, 0);");
					}
				});
			}
		});

		line.setOnMouseExited(new EventHandler() {
			@Override public void handle(Event event) {
				lines.forEach(new Consumer<Line>() {
					@Override public void accept(Line lineFor) {
						lineFor.setStyle(null);
					}
				});
			}
		});
		return line;
	}

	public void setStartRegion(T start){
		startRegion = start;
	}

	public void setEndRegion(T end){
		endRegion = end;
	}

	public void createListeners() {
		Line first = lines.getFirst();

		ChangeListener<Number> fStartXListener = buildEndpointListener(first.startXProperty(), startRegion.layoutXProperty(), startRegion.getPrefWidth());
		startRegion.translateXProperty().addListener(fStartXListener);
		ChangeListener<Number> fStartYListener = buildEndpointListener(first.startYProperty(), startRegion.layoutYProperty(), startRegion.getPrefWidth());
		startRegion.translateYProperty().addListener(fStartYListener);
		ChangeListener<Number> fEndXListener = buildEndpointListener(first.endXProperty(), startRegion.layoutXProperty(), startRegion.getPrefWidth());
		startRegion.translateXProperty().addListener(fEndXListener);

		ChangeListener<Number> lEndYListener = buildEndpointListener(first.endYProperty(), endRegion.layoutYProperty(), endRegion.getPrefWidth());
		endRegion.translateYProperty().addListener(lEndYListener);

		Line last = lines.getLast();

		ChangeListener<Number> startXListener2 = buildEndpointListener(last.startXProperty(), startRegion.layoutXProperty(), endRegion.getPrefWidth());
		startRegion.translateXProperty().addListener(startXListener2);

		ChangeListener<Number> startYListener2 = buildEndpointListener(last.startYProperty(), endRegion.layoutYProperty(), endRegion.getPrefWidth());
		endRegion.translateYProperty().addListener(startYListener2);
		ChangeListener<Number> endXListener2 = buildEndpointListener(last.endXProperty(), endRegion.layoutXProperty(), endRegion.getPrefWidth());
		endRegion.translateXProperty().addListener(endXListener2);
		ChangeListener<Number> endYListener2 = buildEndpointListener(last.endYProperty(), endRegion.layoutYProperty(), endRegion.getPrefWidth());
		endRegion.translateYProperty().addListener(endYListener2);

	}

	@SuppressWarnings({ "rawtypes", "unchecked" }) //shhhhh
	public ChangeListener<Number> buildEndpointListener(DoubleProperty lineProperty, DoubleProperty tableProperty, double tableDimension){
		ChangeListener<Number> listener = new ChangeListener() {
			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
				lineProperty.set(tableProperty.get() + (double)newValue + tableDimension/2);
			}
		};
		return listener;
	}

	public String getStartTableId() {
		return startTableId;
	}

	public void setStartTableId(String startTableId) {
		this.startTableId = startTableId;
	}

	public String getEndTableId() {
		return endTableId;
	}

	public void setEndTableId(String endTableId) {
		this.endTableId = endTableId;
	}
}
