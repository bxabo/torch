package models;

public class Relationship {
	String relationshipId;
	String primaryDatabaseId;
	String primarySchemaId;
	String primaryTableId;
	String primaryFieldId;
	String relatedDatabaseId;
	String relatedSchemaId;
	String relatedTableId;
	String relatedFieldId;
	String javaName;

	public String getRelationshipId() {
		return relationshipId;
	}
	public void setRelationshipId(String relationshipId) {
		this.relationshipId = relationshipId;
	}
	public String getPrimaryTableId() {
		return primaryTableId;
	}
	public void setPrimaryTableId(String primaryTableId) {
		this.primaryTableId = primaryTableId;
	}
	public String getRelatedTableId() {
		return relatedTableId;
	}
	public void setRelatedTableId(String relatedTableId) {
		this.relatedTableId = relatedTableId;
	}
	public String getJavaName() {
		return javaName;
	}
	public void setJavaName(String javaName) {
		this.javaName = javaName;
	}
	public String getPrimaryFieldId() {
		return primaryFieldId;
	}
	public void setPrimaryFieldId(String primaryFieldId) {
		this.primaryFieldId = primaryFieldId;
	}
	public String getRelatedFieldId() {
		return relatedFieldId;
	}
	public void setRelatedFieldId(String relatedFieldId) {
		this.relatedFieldId = relatedFieldId;
	}
	public String getPrimaryDatabaseId() {
		return primaryDatabaseId;
	}
	public void setPrimaryDatabaseId(String primaryDatabaseId) {
		this.primaryDatabaseId = primaryDatabaseId;
	}
	public String getPrimarySchemaId() {
		return primarySchemaId;
	}
	public void setPrimarySchemaId(String primarySchemaId) {
		this.primarySchemaId = primarySchemaId;
	}
	public String getRelatedDatabaseId() {
		return relatedDatabaseId;
	}
	public void setRelatedDatabaseId(String relatedDatabaseId) {
		this.relatedDatabaseId = relatedDatabaseId;
	}
	public String getRelatedSchemaId() {
		return relatedSchemaId;
	}
	public void setRelatedSchemaId(String relatedSchemaId) {
		this.relatedSchemaId = relatedSchemaId;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Relationship [relationshipId=");
		builder.append(relationshipId);
		builder.append(", primarySchemaId=");
		builder.append(primarySchemaId);
		builder.append(", primaryTableId=");
		builder.append(primaryTableId);
		builder.append(", primaryFieldId=");
		builder.append(primaryFieldId);
		builder.append(", relatedSchemaId=");
		builder.append(relatedSchemaId);
		builder.append(", relatedTableId=");
		builder.append(relatedTableId);
		builder.append(", relatedFieldId=");
		builder.append(relatedFieldId);
		builder.append("]");
		return builder.toString();
	}
}
