package models;

import java.util.HashMap;
import java.util.Map;

public class Schema {

	String schemaId;
	String databaseName;
	String descriptiveName;
	Map<String, Table> tables;
	
	public Schema() {
		tables = new HashMap<>();
	}

	public String getSchemaId() {
		return schemaId;
	}
	
	public void setSchemaId(String schemaId) {
		this.schemaId = schemaId;
	}
	
	public String getDatabaseName() {
		return databaseName;
	}
	
	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}
	
	public String getDescriptiveName() {
		return descriptiveName;
	}
	
	public void setDescriptiveName(String descriptiveName) {
		this.descriptiveName = descriptiveName;
	}
	
	public Map<String, Table> getTables() {
		return tables;
	}
	
	public void setTables(Map<String, Table> tables) {
		this.tables = tables;
	}
	
	public void addTable(String tableId, Table table) {
		if(table != null && tableId != null)
			this.tables.put(tableId, table);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Schema [schemaId=");
		builder.append(schemaId);
		builder.append(", databaseName=");
		builder.append(databaseName);
		builder.append("]");
		return builder.toString();
	}
	
}
