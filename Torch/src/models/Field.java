package models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javafx.beans.InvalidationListener;
import javafx.beans.property.ReadOnlyProperty;
import javafx.beans.value.ChangeListener;

public class Field {

	String tableId;
	String fieldId;
	int fieldCount; // because we stored our fields compressed, sometimes... so weird
	String jdbcType;
	String databaseType;
	int databaseLength;
	Class javaType;
	String javaName;
	String databaseName;
	List<Relationship> primaryRelationships;
	List<Relationship> relatedRelationships;
	
	public Field() {
		primaryRelationships = new ArrayList<>();
		relatedRelationships = new ArrayList<>();
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public String getFieldId() {
		return fieldId;
	}

	public void setFieldId(String fieldId) {
		this.fieldId = fieldId;
	}

	public int getFieldCount() {
		return fieldCount;
	}

	public void setFieldCount(int fieldCount) {
		this.fieldCount = fieldCount;
	}

	public String getJdbcType() {
		return jdbcType;
	}

	public void setJdbcType(String jdbcType) {
		this.jdbcType = jdbcType;
	}

	public String getDatabaseType() {
		return databaseType;
	}

	public void setDatabaseType(String databaseType) {
		this.databaseType = databaseType;
	}

	public int getDatabaseLength() {
		return databaseLength;
	}

	public void setDatabaseLength(int databaseLength) {
		this.databaseLength = databaseLength;
	}

	public Class getJavaType() {
		return javaType;
	}

	public void setJavaType(Class javaType) {
		this.javaType = javaType;
	}

	public String getJavaName() {
		return javaName;
	}

	public void setJavaName(String javaName) {
		this.javaName = javaName;
	}

	public String getDatabaseName() {
		return databaseName;
	}
	
	public ReadOnlyProperty<String> databaseNameProperty() {
		ReadOnlyProperty<String> dbName = new ReadOnlyProperty<String>() {
			
			@Override
			public void removeListener(InvalidationListener listener) {}
			
			@Override
			public void addListener(InvalidationListener listener) {}
			
			@Override
			public void removeListener(ChangeListener<? super String> arg0) {}
			
			@Override
			public String getValue() { return getDatabaseName(); }
			
			@Override
			public void addListener(ChangeListener<? super String> arg0) {}
			
			@Override
			public String getName() { return null; }
			
			@Override
			public Object getBean() { return null; }
		};
		return dbName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public Collection<Relationship> getPrimaryRelationships() {
		return primaryRelationships;
	}

	public void setPrimaryRelationships(List<Relationship> relationships) {
		this.primaryRelationships = relationships;
	}

	public void addPrimaryRelationship(Relationship relationship) {
		if(relationship != null)
			this.primaryRelationships.add(relationship); 
	}

	public Collection<Relationship> getRelatedRelationships() {
		return relatedRelationships;
	}

	public void setRelatedRelationships(List<Relationship> relatedRelationships) {
		this.relatedRelationships = relatedRelationships;
	}

	public void addRelatedRelationship(Relationship relatedRelationship) {
		if(relatedRelationship != null)
			this.relatedRelationships.add(relatedRelationship); 
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Field [tableId=");
		builder.append(tableId);
		builder.append(", fieldId=");
		builder.append(fieldId);
		builder.append(", javaName=");
		builder.append(javaName);
		builder.append("]");
		return builder.toString();
	}
}
