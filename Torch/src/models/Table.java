package models;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class Table {
	
	// TODO - to be more generic/flexible, some of these should be in a map of metadata names
	String tableId;
	String databaseName;
	String schemaId;
	String className;
	String prefix;
	String descriptiveName;
	String codebase;
	
	Collection<Relationship> primaryRelationships;
	Collection<Relationship> relatedRelationships;
	Map<String, Field> fields;

	/**
	 * Construction for a Table object.
	 */
	public Table() {
		primaryRelationships = new ArrayList<>();
		relatedRelationships = new ArrayList<>();
		fields = new TreeMap<>();
	}

	public String getTableId() {
		return tableId;
	}

	public void setTableId(String tableId) {
		this.tableId = tableId;
	}

	public String getDatabaseName() {
		return databaseName;
	}

	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}

	public String getSchemaId() {
		return schemaId;
	}

	public void setSchemaId(String schemaId) {
		this.schemaId = schemaId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getDescriptiveName() {
		return descriptiveName;
	}

	public void setDescriptiveName(String descriptiveName) {
		this.descriptiveName = descriptiveName;
	}

	public String getCodebase() {
		return codebase;
	}

	public void setCodebase(String codebase) {
		this.codebase = codebase;
	}

	public Collection<Relationship> getPrimaryRelationships() {
		return primaryRelationships;
	}

	public void setRelationships(Collection<Relationship> relationships) {
		this.primaryRelationships = relationships;
	}

	public void addRelationship(Relationship relationship) {
		if(relationship != null)
			this.primaryRelationships.add(relationship);
	}
	
	public Collection<Relationship> getRelatedRelationships() {
		return relatedRelationships;
	}

	public void setRelatedRelationships(List<Relationship> relatedRelationships) {
		this.relatedRelationships = relatedRelationships;
	}

	public void addRelatedRelationship(Relationship relatedRelationship) {
		if(relatedRelationship != null)
			this.relatedRelationships.add(relatedRelationship); 
	}
	
	public Map<String, Field> getFields() {
		return fields;
	}

	public void setFields(Map<String, Field> fields) {
		this.fields = fields;
	}

	public void addField(String fieldId, Field field) {
		if(field != null && fieldId != null)
		this.fields.put(fieldId, field);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Table [tableId=");
		builder.append(tableId);
		builder.append(", databaseName=");
		builder.append(databaseName);
		builder.append(", prefix=");
		builder.append(prefix);
		builder.append("]");
		return builder.toString();
	}
}
