Torch - Interactive ERD tool 

Torch is a stand-alone tool that helps to create Entity Relationship Diagrams. This app was made to help understand how specific tables interact and are connected with one another. The app is not intended to allow the user to see what data is stored within the database, only the overarching structure of the database itself.


![right click menu on table](/Torch/src/resources/icons/TorchRightMenu.png?raw=true "right click menu on table")

![adding a new table](/Torch/src/resources/icons/TorchAddTable.png?raw=true "adding a new table")

![adding fields](/Torch/src/resources/icons/TorchAddingFields.png?raw=true "adding fields")


This app currently does not feature a data loader and the DataLoader classes must be written and hard coded. 

At the time of this writing, the project has two example DataLoaders that can be used to write other data loaders. Eventually the ability to link to a database and pull out the structure of a database will be implemented. Currently a work in progress.

